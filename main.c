#include "i2c.h"
#include "HD44780.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define BIT(x)	(1 << x)

static char dev[24];
static int err;

static void wrbus(uint8_t data)
{
	err = i2c_wr(dev, 0x27, data, 0, 0);
}

static void delay_func(uint16_t ms)
{
	usleep(ms * 1000);
}

static struct hd44780_conn con = {
	.bus_type = HD44780_BUS_4B,
	.data_shift = 4,
	.rs_pin = BIT(0),
	.en_pin = BIT(2),
	.backlight_pin = BIT(3),
};

static struct hd44780_lcd lcd = {
	.write = wrbus,
	.write16 = 0,
	.delay_func = delay_func,
	.is_backlight_enabled = 1,
	.type = HD44780_TYPE_LCD,
	.font = HD44780_ENGLISH_RUSSIAN_FONT,
	.conn = &con,
	.ext_con = 0,
};

int main(int argc, char** argv)
{
	int dev_num = 1;

	snprintf(dev, sizeof(dev), "/dev/i2c-%d", dev_num);

	hd44780_init(&lcd);

	if (argc >= 2)
		hd44780_print(argv[1]);
	if (argc == 3) {
		hd44780_set_pos(1, 0);
		hd44780_print(argv[2]);
	}

	printf("Done: %s\n", err ? strerror(err) : "ok");

	return err;
}

